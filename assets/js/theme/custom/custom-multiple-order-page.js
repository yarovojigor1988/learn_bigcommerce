import PageManager from '../page-manager'
import get from 'bigcommerce-graphql'
import 'regenerator-runtime/runtime';
import { async } from 'regenerator-runtime';


export default class CustomDemo extends PageManager {
    onReady () {
        
        let productId = this.context.productId

        async function createNewCart(lineItems) {
            const response = await fetch(`/api/storefront/carts`, {
                credentials: "include",
                method: "POST",
                body: JSON.stringify({ lineItems: lineItems })
            });
            const data = await response.json();
            if (!response.ok) {
                return Promise.reject("There was an issue adding items to your cart. Please try again.")
            } else {
                console.log(data);
            }
        }
        
        async function addToExistingCart(cart_id, lineItems) {
            const response = await fetch(`/api/storefront/carts/${cart_id}/items`, {
                credentials: "include",
                method: "POST",
                body: JSON.stringify({ lineItems: lineItems })
            });
            const data = await response.json();
            if (!response.ok) {
                return Promise.reject("There was an issue adding items to your cart. Please try again.")
            } else {
                console.log(data);
            }
        }

        let productVariants = [];

        async function getOptions (cursor) {
            const response = get(`{ site { product(entityId: ${productId}) { id entityId name variants(first: 10 after:"${cursor}") { pageInfo {endCursor hasNextPage} edges { node { id entityId sku prices { price {...MoneyFields} } options{ edges { node { entityId displayName isRequired values { edges { node { entityId label }}} }}} defaultImage {url(width: 80)}}}} }}} fragment MoneyFields on Money {value currencyCode}`).then((data) => {
                return data;
            })
            let data = await response;                
                productVariants = [...productVariants, ...data.site.product.variants];
                
            if (data.site.product.variantspageInfo.hasNextPage) {
                return getOptions(data.site.product.variantspageInfo.endCursor);
            } else {
                return createHtml();
            }
        }

        getOptions();

        function createHtml() {
            let variantsForm = document.getElementById('variantsForm');
            const filterOptionValues = (option_values) => {
                const nameValues = option_values.map((option) => {
                    return `<p><strong>${option.displayName}</strong>:${option.values[0].label}</p>`
                })
                return nameValues.join('');
            };

            productVariants.forEach(variant => {
                let newDiv = document.createElement("div");
                newDiv.classList.add("variants_form_row");
                let imgUrl;
                if (variant.defaultImage == null) {                   
                    imgUrl = "";
                }   else {
                    imgUrl = variant.defaultImage.url;

                }
                newDiv.innerHTML = `
                    <div class=variants_form_cell><img src="${imgUrl}"></div>
                    <div class=variants_form_cell>${variant.sku}</div>
                    <div class=variants_form_cell>${variant.prices.price.value} ${variant.prices.price.currencyCode}</div>
                    <div class=variants_form_cell>${filterOptionValues(variant.options)}</div>
                    <div class=variants_form_cell><input 
                        type='number' 
                        min='0' 
                        placeholder='0' 
                        class='qtyField' 
                        /></div>
                    </div>`

                variantsForm.appendChild(newDiv);
            });

            document.querySelector('#customFormOverlay').style.display="none";               

        }

        function createLine(){
            document.querySelector('.loadingOverlay').style.display="block";               
            let lineitems = [];
            let qtyFields  = Array.prototype.slice.call(document.getElementsByClassName('qtyField'));
            console.log(qtyFields)
            for (const [i, item] of qtyFields.entries()) {
                if (item.value){
                    let lineItem = {"quantity": parseInt(item.value),"product_id": data.site.product.entityId, "variant_id": productVariants[i].entityId}
                    lineitems.push(lineItem);
                }
            }
            console.log(lineitems);

            if (lineitems.length < 1) {
                alert ('Please set the quantity of at least 1 item.');
                return
            } else {
                fetch(`/api/storefront/cart`)
                .then(response => response.json())
                .then(cart => {
                    if (cart.length > 0) {
                        return addToExistingCart(cart[0].id, lineitems)
                    } else {
                        return createNewCart(lineitems)
                    }
                })
                .then(() => window.location = '/cart.php')
                .catch(err => console.log(err));
            }              
        }
        
        document.getElementById('variantsFormSubmit').addEventListener('click', createLine);
    }
}