async function getOptions () {
    const response = await 
    fetch('/graphql',{
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiJ9.eyJjaWQiOjEsImNvcnMiOlsiaHR0cHM6Ly9zdG9yZS02YXFsYzNtam5lLm15YmlnY29tbWVyY2UuY29tIl0sImVhdCI6MTYzMjA0MTIxOSwiaWF0IjoxNjMxODY4NDE5LCJpc3MiOiJCQyIsInNpZCI6MTAwMTk1MDAxNywic3ViIjoiYmNhcHAubGlua2VyZCIsInN1Yl90eXBlIjowLCJ0b2tlbl90eXBlIjoxfQ.NV8asxw2Rz7uYkIZUBzRTTetprwKa_5O2yP8qFWKjm_K8FXAlMzXJ1A40odtsrl5LIGWCokAhW105wC_12HZZw'
        },
        body: JSON.stringify({query: `query MyFirstQuery { site { product(entityId: 117) { id entityId name variants { edges { node { id entityId sku prices { price {...MoneyFields} } options{ edges { node { entityId displayName isRequired values { edges { node { entityId label }}} }}} defaultImage {url(width: 300)}}}} }}} fragment MoneyFields on Money {value currencyCode}`})
    });

    const responseData = await response.json();
    console.log ("responseData");
    console.log (responseData);
    // productVariants.push(responseData.data.site.product.variants);
    let productVariantsPart = responseData.data.site.product.variants.edges.map(responseData => responseData.node)
    productVariants = [...productVariants, ...productVariantsPart]
    console.log ("productVariants");
    console.log (productVariants);

}
getOptions();