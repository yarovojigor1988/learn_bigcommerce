import PageManager from '../page-manager'
import get from 'bigcommerce-graphql'
import 'regenerator-runtime/runtime';


export default class CustomDemo extends PageManager {
    onReady () {

        let target = document.querySelector('#customFormFieldID');

        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
            document.querySelector('.loadingOverlay').style.display="none";
            });    
        });

        let config = { attributes: true, childList: true, characterData: true };
        let lineItems = [];
 
        async function createNewCart(lineItems) {
            const response = await fetch(`/api/storefront/carts`, {
                credentials: "include",
                method: "POST",
                body: JSON.stringify({ lineItems: lineItems })
            });
            const data = await response.json();
            if (!response.ok) {
                return Promise.reject("There was an issue adding items to your cart. Please try again.")
            } else {
                console.log(data);
            }
        }
        
        async function addToExistingCart(cart_id, lineItems) {
            const response = await fetch(`/api/storefront/carts/${cart_id}/items`, {
                credentials: "include",
                method: "POST",
                body: JSON.stringify({ lineItems: lineItems })
            });
            const data = await response.json();
            if (!response.ok) {
                return Promise.reject("There was an issue adding items to your cart. Please try again.")
            } else {
                console.log(data);
            }
        }

        observer.observe(target, config);

        get(`{site { products(entityIds: [104,107,111]) { edges{ node{ id, entityId, name, description, sku, inventory {isInStock}, createdAt {utc} prices { price { value, currencyCode } } defaultImage { url(width:1280) } } } } } }`).then((data) => {

            let products = data.site.products;         
            let totalPrice =0;
            let customCurrencyCode = products[0].prices.price.currencyCode;
            products.forEach(element => {
                element.amount=1;
                
                let description = element.description;
                let elementDescription;
                if (description.length >= 200) {
                    elementDescription = description.slice(3 ,200) + "...";
                } else {
                    elementDescription = description;
                }
                totalPrice+= element.prices.price.value;
                let elementPrice = element.prices.price.value + " " + element.prices.price.currencyCode;
                document.getElementById('customFormFieldID').innerHTML += `
                <div style="max-width: 31%; padding: 0 5px; box-shadow:2px 2px 5px 2px rgba(0, 0, 0, 0.2); margin-bottom:20px">
                    <div style="padding:0 10px">
                        <h2>${element.name}</h2>
                    </div>
                    <div>
                        <img src="${element.defaultImage.url}" alt="custom form field product">
                    </div>
                    <div style="padding:10px">
                        <input class="form-input custom-form-field-num" type="text" id="attribute_number_${element.entityId}" step="any" name="attribute[${element.entityId}]">
                    </div>
                    <div style="padding:0 10px 10px">
                        ${elementDescription}
                    </div>
                    <div style="padding:0 10px 10px">
                        ${elementPrice}
                    </div>
                </div>
                `

            });

            document.getElementById('customFormSubID').addEventListener('click', function(e){
                e.preventDefault();
                document.querySelector('.loadingOverlay').style.display="block";               
                function customaddToCart(products){
                    for (const product of products){

                        let lineItem = {"quantity": product.amount ,"product_id": product.entityId }
                        lineItems.push(lineItem);
                    }
                    
                    fetch(`/api/storefront/cart`)
                    .then(response => response.json())
                    .then(cart => {
                        if (cart.length > 0) {
                            return addToExistingCart(cart[0].id, lineItems)
                        } else {
                            return createNewCart(lineItems)
                        }
                    })
                    .then(() => window.location = '/cart.php')
                    .catch(err => console.log(err));
                   
                }
                customaddToCart(products);
            });

            document.getElementById('customFormFieldHolderID').innerHTML += `<div id="priceCustomTotal"> Total Price: ${totalPrice} ${customCurrencyCode}</div>`;

            let customAmount = document.getElementsByClassName('custom-form-field-num');

            function changeCustomprice(){
                let changedCustomPrice = 0;
                data.site.products.forEach(element => {
                    changedCustomPrice += element.prices.price.value*element.amount;
                    changedCustomPrice = Math.floor(changedCustomPrice * 100) / 100;
                    document.getElementById('priceCustomTotal').innerHTML = `Total Price: ${changedCustomPrice} ${customCurrencyCode}`
                })
            }
            for (let i=0; i<customAmount.length; i++){
                customAmount[i].addEventListener('change', function(){
                    let patternstr = '^[0-9]+$';
                    let constraint = new RegExp(patternstr, "");

                    if (customAmount[i].value > 10 || customAmount[i].value < 1) {
                        customAmount[i].setCustomValidity("Please input correct number - from 1 to 10");
                        customAmount[i].dataset.validated="false";
                    } else if (constraint.test(customAmount[i].value)) {
                    customAmount[i].setCustomValidity("");
                    customAmount[i].dataset.validated="true";
                    } else {
                        customAmount[i].setCustomValidity("Rrestricted symbol.");
                        customAmount[i].dataset.validated="false";             
                    }
                });
                customAmount[i].addEventListener('focusout', function(){
                    customAmount[i].reportValidity();
                    if (customAmount[i].reportValidity()) {                    
                        customAmount[i].style.borderColor = "inherit";
                        if (customAmount[i].value >= 1) {
                            data.site.products[i].amount = customAmount[i].value;
                            changeCustomprice();
                        }
                    }   else {
                        customAmount[i].style.borderColor = "red";
                    }
                });
            }
        });
    }
}
