import 'regenerator-runtime/runtime';
import PageManager from '../page-manager'

export default class CustomDemo extends PageManager {
    onReady() {

        let submitButtonId;

        if (document.getElementById("engraving_options_added")) {
            document.getElementById("form-action-addToCart").id = "form-action-addToCart-changed";
            submitButtonId = "form-action-addToCart-changed";
        } else {
            submitButtonId = "form-action-addToCart";
        }   

        let giftProdId;
        let giftOptionId;   
        if (this.context.giftOptionId){
            giftOptionId = "attribute_text_" + this.context.giftOptionId;
            giftProdId =this.context.giftOptionId;
        }

        let globalEngravingOptionId;
        if (this.context.engravingOptionId){
            globalEngravingOptionId = "attribute_text_" + this.context.engravingOptionId;            
        }   
           
        let productPrice = this.context.customProduct.price.without_tax.value;
        let customProductId = this.context.customProduct.id;
        let priseChangeOptionId;
        let priseChangeValueArr;
        let priseChangeValueId;

        function clearAlertMassege() {
            let elements = document.getElementsByClassName("customAlertMessage");
            for (Element of elements) {
                Element.remove();
            }
        }

        function addAlertMassege(element, alertText) {
            clearAlertMassege();

            let newDiv = document.createElement("div");
            if (element.id == giftOptionId){
                newDiv.style.top = "80px";
                newDiv.style.position = "absolute";
                newDiv.style.width = "100%";
            }
            newDiv.innerHTML = `<div class="customAlertMessage" style="max-width: 90%;display: block;position: absolute;padding: 10px;color: red;background: white;border: 1px solid red;border-radius: 5px;transform: translateY(5px);">${alertText}</div>`;
            element.parentNode.appendChild(newDiv);
            setTimeout(clearAlertMassege, 5000);
        }

        
        if (this.context.customProduct.options.filter(option => option.display_name === "Engraving price")[0]) {
            priseChangeOptionId = this.context.customProduct.options.filter(option => option.display_name === "Engraving price")[0].id;
            priseChangeValueArr = this.context.customProduct.options.filter(option => option.display_name === "Engraving price")[0].values
            priseChangeValueId = `""`;
        }

        if (document.getElementById("custom_options_added")) {
            
            function showGiftTextField() {
                let element = document.getElementById('giftFieldForm');
                element.style.display = "block";
                document.getElementById(giftOptionId).setAttribute("required", "");
                element.required = true;
            }

            function hideGiftTextField() {
                document.getElementById('giftFieldForm').style.display = "none";
                document.getElementById(giftOptionId).removeAttribute("required");
                document.getElementById('attribute_text_custom_email').removeAttribute("required");
            }

            function showGiftEmailTextField() {
                let element = document.getElementById('GiftEmailField');
                element.style.display = "block";
                document.getElementById('attribute_text_custom_email').setAttribute("required", "");
                element.required = true;
            }

            function hideGiftEmailField() {
                let element = document.getElementById('GiftEmailField');
                element.style.display = "none";
                document.getElementById('attribute_text_custom_email').removeAttribute("required");
                document.getElementById('attribute_text_custom_email').value = "";

            }

            function checkGiftText() {
                let patternstr = '^[A-Za-z0-9., \n]{0,300}$';
                let giftText = document.getElementById(giftOptionId);
                let constraint = new RegExp(patternstr, "");
                if (giftText.value.length > 200) {

                    giftText.setCustomValidity("Too much symbols. Please, make your text shorter. 200 symbols maximum.");                 
                    let alertText = "Too much symbols. Please, make your text shorter. 200 symbols maximum.";
                    addAlertMassege(giftText, alertText);
                    giftText.dataset.validated = "false";
                    giftText.style.border = "2px solid red";
                    document.getElementById(submitButtonId).disabled = true;


                } else if (constraint.test(giftText.value)) {
                    giftText.setCustomValidity("");
                    giftText.dataset.validated = "true";
                    clearAlertMassege();
                    giftText.style.border = "1px solid #8f8f8f";
                    document.getElementById(submitButtonId).disabled = false;
                    

                }
                else {
                    giftText.setCustomValidity("Unknown or restricted symbol: A congratulatory inscription should consist of letters, commas, and dots.");
                    let alertText = "Unknown or restricted symbol: A congratulatory inscription should consist of letters, commas, and dots.";
                    addAlertMassege(giftText, alertText);
                    giftText.dataset.validated = "false";
                    giftText.style.border = "2px solid red";
                    document.getElementById(submitButtonId).disabled = true;
                }
            }

            function checkCustomMailText() {
                let patternstr = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
                let giftMailText = document.getElementById("attribute_text_custom_email");
                let constraint = new RegExp(patternstr, "");
                if (constraint.test(giftMailText.value)) {
                    giftMailText.dataset.validated = "true";
                    giftMailText.setCustomValidity("");
                    clearAlertMassege();
                    giftMailText.style.border = "1px solid #8f8f8f";
                    document.getElementById(submitButtonId).disabled = false;

                }
                else {
                    giftMailText.dataset.validated = "false";
                    giftMailText.setCustomValidity("Please enter a valid email address.");
                    let alertText = "Please enter a valid email address.";
                    addAlertMassege(giftMailText, alertText);
                    giftMailText.dataset.validated = "false";
                    giftMailText.style.border = "2px solid red";
                    document.getElementById(submitButtonId).disabled = true;
                }
            }

            function clearCustomData() {
                document.getElementById('show_gift_option').checked = false;
                document.getElementById(giftOptionId).value = "";
                document.getElementById('not_show_gift_option').checked = true;
                document.getElementById('show_gift_email_option').checked = false;
                document.getElementById('attribute_text_custom_email').value = "";
                document.getElementById('not_show_gift_email').checked = true;
                hideGiftTextField();
                hideGiftEmailField();
            }

            function checkCloseCart(e) {

                if (e.target.classList.contains('modal-close') || e.target.parentElement.classList.contains('modal-close')) {
                    clearCustomData();
                };
                if (e.target.dataset.revealClose === "") {
                    clearCustomData();
                }
            }

            function checkCloseCart2(e) {
                if (e.target.classList.contains('modal-background') && document.getElementById('previewModal').style.display == "block") {
                    clearCustomData();
                }
            }

            clearCustomData();

            document.getElementById("attribute_text_custom_email").onchange = checkCustomMailText;
            document.getElementById(giftOptionId).onchange = checkGiftText;
            document.getElementById("not_show_gift_option").addEventListener('change', hideGiftTextField, false);
            document.getElementById("show_gift_option").addEventListener('change', showGiftTextField, false);
            document.getElementById("not_show_gift_email").addEventListener('change', hideGiftEmailField, false);
            document.getElementById("show_gift_email_option").addEventListener('change', showGiftEmailTextField, false);

            function checkGiftTextSubmitted(event) {
                // event.preventDefault();

                if (document.getElementById('not_show_gift_option').checked === true) {
                    document.getElementById(giftOptionId).value = "";

                } else if (document.getElementById('show_gift_email_option').checked === false
                && document.getElementById(giftOptionId).value !== ""
                && document.getElementById(giftOptionId).dataset.validated === "true" 
                && !document.getElementById("show_engraving_option")) {
                document.getElementById(giftOptionId).value += " Print congratulatory on a gift card";
                
                } else if (document.getElementById('show_gift_email_option').checked === false
                    && document.getElementById(giftOptionId).value !== ""
                    && document.getElementById(giftOptionId).dataset.validated === "true"
                    && document.getElementById("show_engraving_option")
                    && !document.getElementById("show_engraving_option").checked) {
                    document.getElementById(giftOptionId).value += " Print congratulatory on a gift card";

                } else if (document.getElementById('show_gift_email_option').checked === false
                    && document.getElementById(giftOptionId).value !== ""
                    && document.getElementById(giftOptionId).dataset.validated === "true"
                    && document.getElementById(globalEngravingOptionId).value.length > 0
                    && document.getElementById(globalEngravingOptionId).dataset.validated === "true") {
                    document.getElementById(giftOptionId).value += " Print congratulatory on a gift card";

                } else if (document.getElementById('show_gift_email_option').checked === true && document.getElementById(giftOptionId).value !== "" && document.getElementById("attribute_text_custom_email").dataset.validated === "true") {
                    document.getElementById(giftOptionId).value +=  " Send congratulatory on Email:" + document.getElementById('attribute_text_custom_email').value;

                }

                document.body.addEventListener('click', checkCloseCart2, false);
            }
            document.getElementById(submitButtonId).addEventListener('click', checkGiftTextSubmitted, false);
            document.getElementById("previewModal").addEventListener('click', checkCloseCart, false);
        }

        if (document.getElementById("engraving_options_added")) {

            let engravProdId = this.context.engravingOptionId;

            let engravingOptionId = "attribute_text_" + this.context.engravingOptionId;

            let engravingPriceId = "attribute_select_" + this.context.engravingPriceId;

            function checkengravingText() {

                let patternstr = '^[A-Za-z0-9., \n]{0,200}$';
                let engravingText = document.getElementById(engravingOptionId);
                let constraint = new RegExp(patternstr, "");
                console.log(engravingText.value);
                if (engravingText.value.length > 50) {
                    let alertText = "Too much symbols. Please, make your text shorter. 50 symbols maximum.";
                    addAlertMassege(engravingText, alertText);
                    engravingText.dataset.validated = "false";
                    engravingText.style.border = "2px solid red";
                    document.getElementById(submitButtonId).disabled = true;

                } else if (engravingText.value.length < 1) {
                        let alertText = "Please, add your text.";
                        addAlertMassege(engravingText, alertText);
                        engravingText.dataset.validated = "false";
                        engravingText.style.border = "2px solid red";
                        document.getElementById(submitButtonId).disabled = true;

                } else if (constraint.test(engravingText.value)) {
                    engravingText.setCustomValidity("");
                    engravingText.dataset.validated = "true";
                    clearAlertMassege();
                    engravingText.style.border = "1px solid #8f8f8f";
                    document.getElementById(submitButtonId).disabled = false;
                }
                else {
                    let alertText = "Unknown or restricted symbol: A congratulatory inscription should consist of letters, commas, and dots.";
                    addAlertMassege(engravingText, alertText);
                    engravingText.dataset.validated = "false";
                    engravingText.style.border = "2px solid red";
                    document.getElementById(submitButtonId).disabled = true;
                }
            }

            function showEngravingTextField() {
                let element = document.getElementById('engravingFieldForm');
                element.style.display = "block";
                document.getElementById(engravingOptionId).setAttribute("required", "");
                element.required = true;
            }

            function hideEngravingTextField() {
                document.getElementById('engravingFieldForm').style.display = "none";
                document.getElementById(engravingOptionId).value = "";                
                document.getElementById(engravingOptionId).removeAttribute("required");
                document.getElementById(submitButtonId).disabled = false;
            }

            function changeEngravingPrice() {
                let length1 = document.getElementById(engravingOptionId).value.replace(/\s+/g, '').length;
                if (length1 > 0 && length1 <= 50){
                    priseChangeValueId = priseChangeValueArr[length1-1].id;
                } else {
                    priseChangeValueId = `""`;
                }
                let newPriceNode = document.querySelector('.productView-price');
                newPriceNode.innerHTML = "₴";
                let newPrice = (productPrice + length1*2).toFixed(2);
                newPriceNode.innerHTML += newPrice;
            }

            document.getElementById("not_show_engraving_option").addEventListener('change', hideEngravingTextField, false);
            document.getElementById("show_engraving_option").addEventListener('change', showEngravingTextField, false);

            let option1 = document.getElementById(engravingOptionId);


            option1.addEventListener('change', function () {               
                changeEngravingPrice();
                checkengravingText();
            });

            option1.addEventListener('keyup', function () {            
                changeEngravingPrice();
                setTimeout(checkengravingText, 3000);
            });

            document.getElementById(submitButtonId).addEventListener('click', checkGiftTextSubmitted, false);

            function checkGiftTextSubmitted(event) {
                event.preventDefault();
                async function createNewCart(lineItems) {
                    const response = await fetch(`/api/storefront/carts`, {
                        credentials: "include",
                        method: "POST",
                        body: lineItems
                    });
                    const data = await response.json();
                    if (!response.ok) {
                        return Promise.reject("There was an issue adding items to your cart. Please try again.")
                    } else {
                        console.log(data);
                    }
                }
                async function addToExistingCart(cart_id, lineItems) {
                    const response = await fetch(`/api/storefront/carts/${cart_id}/items`, {
                        credentials: "include",
                        method: "POST",
                        body: lineItems
                    });
                    const data = await response.json();
                    if (!response.ok) {
                        return Promise.reject("There was an issue adding items to your cart. Please try again.")
                    } else {
                        console.log(data);
                    }
                }

                function checkEngravingField() {
                    let engravingText = document.getElementById(engravingOptionId);
                    let alertText = "Please add your engraving text";
                    addAlertMassege(engravingText, alertText);
                    engravingText.dataset.validated = "false";
                    engravingText.style.border = "2px solid red";
                    document.getElementById(submitButtonId).disabled = true;                    
                }

                function checkGiftField(element, alertText) {
                    addAlertMassege(element, alertText);

                    element.dataset.validated = "false";
                    element.style.border = "2px solid red";
                    document.getElementById(submitButtonId).disabled = true;
                }

                let itemQuantity = document.getElementById('qty[]').value;
                let itemengravingOptin = document.getElementById(globalEngravingOptionId).value;  
                let lineItems;
               
                if (document.getElementById("show_engraving_option").checked && itemengravingOptin.length < 1) {
                    checkEngravingField();
                    return;                
                }

                if (giftOptionId) {
                    let itemgiftOptin = document.getElementById(giftOptionId).value;

                    if (document.getElementById("show_gift_option").checked && itemgiftOptin.length < 1) {
                        let element = document.getElementById(giftOptionId);
                        let alertText = "Please add your text.";
                        checkGiftField(element, alertText);
                        return;                
                    }


                    if (document.getElementById("show_gift_email_option").checked && document.getElementById("attribute_text_custom_email").value.length < 1) {
                        let element = document.getElementById("attribute_text_custom_email");
                        let alertText = "Please add your email.";
                        checkGiftField(element, alertText);
                        return;                
                    }

                    lineItems = `{"lineItems":[{"productId": ${customProductId}, "quantity": ${itemQuantity}, "option_selections": [{"option_id": ${giftProdId}, "optionValue": "${itemgiftOptin}"}, {"option_id": ${engravProdId}, "optionValue": "${itemengravingOptin}"}, { "option_id": ${priseChangeOptionId}, "optionValue": ${priseChangeValueId}}]}]}`;
                } else if (document.getElementById("show_engraving_option").checked){
                    lineItems = `{"lineItems":[{"productId": ${customProductId}, "quantity": ${itemQuantity}, "option_selections": [{"option_id": ${engravProdId}, "optionValue": "${itemengravingOptin}"}, { "option_id": ${priseChangeOptionId}, "optionValue": ${priseChangeValueId}}]}]}`;
                } else {
                    lineItems = `{"lineItems":[{"productId": ${customProductId}, "quantity": ${itemQuantity}}]}`;
                }
                
                fetch(`/api/storefront/cart`)
                    .then(response => response.json())
                    .then(cart => {
                        if (cart.length > 0) {
                            return addToExistingCart(cart[0].id, lineItems)
                        } else {
                            return createNewCart(lineItems)
                        }
                    })
                    .then(() => window.location = '/cart.php')
                    .catch(err => console.log(err));
            }
        }
    }
}
